package exampleproject;

import javafx.fxml.FXML;
import util.ViewUtil;
import util.PopupUtil;
import util.StageContainer;

public class ExampleView2Controller extends StageContainer {

    @FXML
    private void handlePopupClick() {
        PopupUtil.<PopupController>createPopup("exampleproject/Popup.fxml", this.getStage(),
                (popup, controller) -> controller.doSomething("Hei fra den andre controlleren!"));
    }

    @FXML
    private void handleContextClick() {
        ViewUtil.switchView("exampleproject/App1.fxml", this.getStage(), controller -> {
        });
    }
}
